package facci.sextimoa.reaml_practica;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

import io.realm.Realm;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Realm realm;
    private TextView output;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        realm=Realm.getDefaultInstance();
        Button insert=findViewById(R.id.insert);
        Button update=findViewById(R.id.update);
        Button read=findViewById(R.id.read);
        Button delete=findViewById(R.id.delete);
        output=findViewById(R.id.show_data);

        insert.setOnClickListener(this);
        update.setOnClickListener(this);
        read.setOnClickListener(this);
        delete.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.insert){
            Log.d("Insert","Insert");
            ShowInsertDialog();
        }
        if(v.getId()==R.id.update){
            showUpdateDialog();
            Log.d("Insert","Insert");
        }
        if(v.getId()==R.id.delete){
            Log.d("Insert","Insert");
            ShowDeleteDialog();
        }
        if(v.getId()==R.id.read){
            showData();
            Log.d("Insert","Insert");
        }
    }
// Metodo para mostrar modal flotante de actulizar un registro
    //Este solo es para seleccionar el registro por ID
    private void showUpdateDialog() {
        // Se reutiliza la misma interfas para para los prosesos de eliminar y actualizar
        final AlertDialog.Builder al=new AlertDialog.Builder(MainActivity.this);
        View view=getLayoutInflater().inflate(R.layout.delete_dialog,null);
        al.setView(view);

        final EditText data_id=view.findViewById(R.id.data_id);
        Button delete=view.findViewById(R.id.delete);
        final AlertDialog alertDialog=al.show();

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                long id= Long.parseLong(data_id.getText().toString());
                final DataModel dataModel=realm.where(DataModel.class).equalTo("id",id).findFirst();
                ShowUpdateDialog(dataModel);
            }
        });
    }
// Metodo para utilizar el modal flotante para eliminar un registro
    private void ShowDeleteDialog() {
        AlertDialog.Builder al=new AlertDialog.Builder(MainActivity.this);
        View view=getLayoutInflater().inflate(R.layout.delete_dialog,null);
        al.setView(view);

        final EditText data_id=view.findViewById(R.id.data_id);
        Button delete=view.findViewById(R.id.delete);
        final AlertDialog alertDialog=al.show();

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long id= Long.parseLong(data_id.getText().toString());
                final DataModel dataModel=realm.where(DataModel.class).equalTo("id",id).findFirst();
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        alertDialog.dismiss();
                        dataModel.deleteFromRealm();
                    }
                });
            }
        });
    }
// Metodo para utilizar el modal flotante para Insertar o crear  un registro
    private void ShowInsertDialog() {
        //Muestra el modal flotante
        AlertDialog.Builder al=new AlertDialog.Builder(MainActivity.this);
        View view=getLayoutInflater().inflate(R.layout.data_input_dialog,null);
        al.setView(view);
//obtenemos la informacion ingresada
        final EditText name=view.findViewById(R.id.name);
        final EditText age=view.findViewById(R.id.age);
        final Spinner gender=view.findViewById(R.id.gender);
        Button save=view.findViewById(R.id.save);
        final AlertDialog alertDialog=al.show();

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

                final DataModel dataModel=new DataModel();

                Number current_id=realm.where(DataModel.class).max("id");
                long nextId;
                if(current_id==null){
                    nextId=1;
                }
                else{
                    nextId=current_id.intValue()+1;
                }

                dataModel.setId(nextId);
                dataModel.setAge(Integer.parseInt(age.getText().toString()));
                dataModel.setName(name.getText().toString());
                dataModel.setGender(gender.getSelectedItem().toString());

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.copyToRealm(dataModel);
                    }
                });
            }
        });

    }
// Metodo para actulizar los campos del registro
    private void ShowUpdateDialog(final DataModel dataModel) {
        AlertDialog.Builder al=new AlertDialog.Builder(MainActivity.this);
        View view=getLayoutInflater().inflate(R.layout.data_input_dialog,null);
        al.setView(view);

        final EditText name=view.findViewById(R.id.name);
        final EditText age=view.findViewById(R.id.age);
        final Spinner gender=view.findViewById(R.id.gender);
        Button save=view.findViewById(R.id.save);
        final AlertDialog alertDialog=al.show();

        name.setText(dataModel.getName());
        age.setText(""+dataModel.getAge());
        if(dataModel.getGender().equalsIgnoreCase("Male")) {
            gender.setSelection(0);
        }
        else{
            gender.setSelection(1);
        }

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();


                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        dataModel.setAge(Integer.parseInt(age.getText().toString()));
                        dataModel.setName(name.getText().toString());
                        dataModel.setGender(gender.getSelectedItem().toString());
                        realm.copyToRealmOrUpdate(dataModel);
                    }
                });
            }
        });

    }

// Metodo que lee los datos  ingresados en la tabla
    private void showData(){
        List<DataModel> dataModels=realm.where(DataModel.class).findAll();
        output.setText("");
        for(int i=0;i<dataModels.size();i++){
            output.append("\nID : "+dataModels.get(i).getId()+"\n Name/Nombre: "+dataModels.get(i).getName()+"\n Age/Edad : "+dataModels.get(i).getAge()+"\n Gender/Genero : "+dataModels.get(i).getGender()+" \n");
        }
    }
}